# Blagonic Brothers Snippets
Collection of most important snippets for Sublime Text. Easily generate chunks of HTML (widgets, tables, forms etc.) and CSS code.

## HTML Snippet codes
* [com] - Section comment
* [pagination] - Creates standard pagination
* [slider] - Creates slider container and first slider item
* [slideritem] - Creates single slider item
* [msg] - Creates message markup
* [main] - Creates #main content with .main-content and .secondary-content blocks
* [promo] - #promo placeholder
* [postlisting] - Post listing <article> with necessary meta elements 
* [postfull] - Single post
* [navigation] - Standard navigation markup
* [rwdheadnav] - Standard responsive-ready navigation
* [normalform] - Standard form markup with input field and submit button
* [forminput] - Additional input field
* [formselect] - Additional select field
* [icon] - Icon markup with aria-role="hidden"
* [fimg] - Image markup with site_url path

## CSS Snippets
* [tsa] - Text shadow + RGBA
* [rgba] - RGBA
* [br] - Border radius (no LESS)
* [bs] - Box shadow (no LESS)

## PHP Snippets
* [phploop] - Standard loop for repeating content

## Soon™
* [wgtarchive] - Widget post listing
* [normaltable] - Normal table markup 
* [searchform] - Search form markup
* [loginform] - Login form markup